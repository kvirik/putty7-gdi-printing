/*
 * Printing interface for PuTTY.
 *  - GDI Printing by Kvirikadze Vitaly (Ukraine, Dnipro).
 */

#include "putty.h"
#include <winspool.h>
#include <math.h>

#define FIXED_TABS 256

#define ftLarge     8
#define ftCondens   4
#define ftBold      2 
#define ftUnderline 1

#define ftNoLarge     7
#define ftNoCondens   11
#define ftNoBold      13
#define ftNoUnderline 14

#define ftN    0
#define ftNU   1
#define ftNB   2
#define ftNUB  3
#define ftC    4
#define ftCU   5
#define ftCB   6
#define ftCUB  7
#define ftL    8
#define ftLU   9
#define ftLB   10
#define ftLUB  11

typedef struct info_gdi_print_tag {
	FLOAT xScale;
	FLOAT yScale;
	HDC printerDC;
	TEXTMETRIC textmetric;
	DOCINFO docInfo;
	FLOAT fontSize;
	DWORD bitsPerInchX;
	DWORD bitsPerInchY;
	HFONT fontN;
	HFONT fontC;
	HFONT fontL;
	HFONT fontNU;
	HFONT fontCU;
	HFONT fontLU;
	HFONT fontNB;
	HFONT fontCB;
	HFONT fontLB;
	HFONT fontNUB;
	HFONT fontCUB;
	HFONT fontLUB;
	short fontType;
	int nInternalFontSize;
	int cFontWidth;
	int nFontWidth;
	int lFontWidth;
	int cFontHeight;
	int nFontHeight;
	int lFontHeight;
	int paperWidth;
	int paperHeight;
	int offsetX;
	int offsetY;
	int posY;
	int posX;
	int pixelPosX;
	int tenPos[FIXED_TABS];
	BOOL undercmd;
	BOOL esc;
} InfoGDIPrint;

struct printer_enum_tag {
    int nprinters;
    DWORD enum_level;
    union {
	LPPRINTER_INFO_4 i4;
	LPPRINTER_INFO_5 i5;
    } info;
};

struct printer_job_tag {
    HANDLE hprinter;
    InfoGDIPrint *GDIPrint;
};

DECL_WINDOWS_FUNCTION(static, BOOL, EnumPrinters,
                      (DWORD, LPTSTR, DWORD, LPBYTE, DWORD, LPDWORD, LPDWORD));
DECL_WINDOWS_FUNCTION(static, BOOL, OpenPrinter,
                      (LPTSTR, LPHANDLE, LPPRINTER_DEFAULTS));
DECL_WINDOWS_FUNCTION(static, BOOL, ClosePrinter, (HANDLE));
DECL_WINDOWS_FUNCTION(static, DWORD, StartDocPrinter, (HANDLE, DWORD, LPBYTE));
DECL_WINDOWS_FUNCTION(static, BOOL, EndDocPrinter, (HANDLE));
DECL_WINDOWS_FUNCTION(static, BOOL, StartPagePrinter, (HANDLE));
DECL_WINDOWS_FUNCTION(static, BOOL, EndPagePrinter, (HANDLE));
DECL_WINDOWS_FUNCTION(static, BOOL, WritePrinter,
                      (HANDLE, LPVOID, DWORD, LPDWORD));

static void init_winfuncs(void)
{
    static int initialised = FALSE;
    if (initialised)
        return;
    {
        HMODULE winspool_module = load_system32_dll("winspool.drv");
        /* Some MSDN documentation claims that some of the below functions
         * should be loaded from spoolss.dll, but this doesn't seem to
         * be reliable in practice.
         * Nevertheless, we load spoolss.dll ourselves using our safe
         * loading method, against the possibility that winspool.drv
         * later loads it unsafely. */
        (void) load_system32_dll("spoolss.dll");
        GET_WINDOWS_FUNCTION_PP(winspool_module, EnumPrinters);
        GET_WINDOWS_FUNCTION_PP(winspool_module, OpenPrinter);
        GET_WINDOWS_FUNCTION_PP(winspool_module, ClosePrinter);
        GET_WINDOWS_FUNCTION_PP(winspool_module, StartDocPrinter);
        GET_WINDOWS_FUNCTION_PP(winspool_module, EndDocPrinter);
        GET_WINDOWS_FUNCTION_PP(winspool_module, StartPagePrinter);
        GET_WINDOWS_FUNCTION_PP(winspool_module, EndPagePrinter);
        GET_WINDOWS_FUNCTION_PP(winspool_module, WritePrinter);
    }
    initialised = TRUE;
}

static int printer_add_enum(int param, DWORD level, char **buffer,
                            int offset, int *nprinters_ptr)
{
    DWORD needed = 0, nprinters = 0;

    init_winfuncs();

    *buffer = sresize(*buffer, offset+512, char);

    /*
     * Exploratory call to EnumPrinters to determine how much space
     * we'll need for the output. Discard the return value since it
     * will almost certainly be a failure due to lack of space.
     */
    p_EnumPrinters(param, NULL, level, (LPBYTE)((*buffer)+offset), 512,
                   &needed, &nprinters);

    if (needed < 512)
        needed = 512;

    *buffer = sresize(*buffer, offset+needed, char);

    if (p_EnumPrinters(param, NULL, level, (LPBYTE)((*buffer)+offset),
                       needed, &needed, &nprinters) == 0)
        return FALSE;

    *nprinters_ptr += nprinters;

    return TRUE;
}

printer_enum *printer_start_enum(int *nprinters_ptr)
{
    printer_enum *ret = snew(printer_enum);
    char *buffer = NULL;

    *nprinters_ptr = 0;		       /* default return value */
    buffer = snewn(512, char);

    /*
     * Determine what enumeration level to use.
     * When enumerating printers, we need to use PRINTER_INFO_4 on
     * NT-class systems to avoid Windows looking too hard for them and
     * slowing things down; and we need to avoid PRINTER_INFO_5 as
     * we've seen network printers not show up.
     * On 9x-class systems, PRINTER_INFO_4 isn't available and
     * PRINTER_INFO_5 is recommended.
     * Bletch.
     */
    if (osVersion.dwPlatformId != VER_PLATFORM_WIN32_NT) {
	ret->enum_level = 5;
    } else {
	ret->enum_level = 4;
    }

    if (!printer_add_enum(PRINTER_ENUM_LOCAL | PRINTER_ENUM_CONNECTIONS,
                          ret->enum_level, &buffer, 0, nprinters_ptr))
        goto error;

    switch (ret->enum_level) {
      case 4:
	ret->info.i4 = (LPPRINTER_INFO_4)buffer;
	break;
      case 5:
	ret->info.i5 = (LPPRINTER_INFO_5)buffer;
	break;
    }
    ret->nprinters = *nprinters_ptr;
    
    return ret;

    error:
    sfree(buffer);
    sfree(ret);
    *nprinters_ptr = 0;
    return NULL;
}

char *printer_get_name(printer_enum *pe, int i)
{
    if (!pe)
	return NULL;
    if (i < 0 || i >= pe->nprinters)
	return NULL;
    switch (pe->enum_level) {
      case 4:
	return pe->info.i4[i].pPrinterName;
      case 5:
	return pe->info.i5[i].pPrinterName;
      default:
	return NULL;
    }
}

void printer_finish_enum(printer_enum *pe)
{
    if (!pe)
	return;
    switch (pe->enum_level) {
      case 4:
	sfree(pe->info.i4);
	break;
      case 5:
	sfree(pe->info.i5);
	break;
    }
    sfree(pe);
}

printer_job *printer_start_job(char *printer)
{
    printer_job *ret = snew(printer_job);
    DOC_INFO_1 docinfo;
    int jobstarted = 0, pagestarted = 0;

    init_winfuncs();

    ret->hprinter = NULL;
    if (!p_OpenPrinter(printer, &ret->hprinter, NULL))
	goto error;

    docinfo.pDocName = "PuTTY remote printer output";
    docinfo.pOutputFile = NULL;
    docinfo.pDatatype = "RAW";

    if (!p_StartDocPrinter(ret->hprinter, 1, (LPBYTE)&docinfo))
	goto error;
    jobstarted = 1;

    if (!p_StartPagePrinter(ret->hprinter))
	goto error;
    pagestarted = 1;

    return ret;

    error:
    if (pagestarted)
	p_EndPagePrinter(ret->hprinter);
    if (jobstarted)
	p_EndDocPrinter(ret->hprinter);
    if (ret->hprinter)
	p_ClosePrinter(ret->hprinter);
    sfree(ret);
    return NULL;
}

void printer_job_data(printer_job *pj, void *data, int len)
{
    DWORD written;

    if (!pj)
	return;

    p_WritePrinter(pj->hprinter, data, len, &written);
}

void printer_finish_job(printer_job *pj)
{
    if (!pj)
	return;

    p_EndPagePrinter(pj->hprinter);
    p_EndDocPrinter(pj->hprinter);
    p_ClosePrinter(pj->hprinter);
    sfree(pj);
}

/* Starting GDI printing */
printer_job *printer_start_job_gdi(char *printer)
{
	/* Static parameters fo font creating */
	int outPrecis = OUT_DEFAULT_PRECIS;
    int clipPrecis = CLIP_DEFAULT_PRECIS;
    int fontQuality = PROOF_QUALITY;
    int fontPitch = FIXED_PITCH;
	int charSet = OEM_CHARSET;
	char *fontName = "Courier New";
    int fontSize = 11;

	/* Flags and stsus */
	int docstarted = 0, pagestarted = 0;
    int status = 0;
	int i;

	/* Creating GDI printing context */
	printer_job *ret = snew(printer_job);
    ret->GDIPrint = snew(InfoGDIPrint);
    ret->GDIPrint->xScale = 1.0f;
    ret->GDIPrint->yScale = 1.0f;
    ret->GDIPrint->printerDC = NULL;
	ret->GDIPrint->fontC = NULL;
    ret->GDIPrint->fontL = NULL;
	ret->GDIPrint->fontNB = NULL;
    ret->GDIPrint->fontCB = NULL;
    ret->GDIPrint->fontLB = NULL;
    ret->GDIPrint->fontNU = NULL;
    ret->GDIPrint->fontCU = NULL;
    ret->GDIPrint->fontLU = NULL;
    ret->GDIPrint->fontNUB = NULL;
    ret->GDIPrint->fontCUB = NULL;
    ret->GDIPrint->fontLUB = NULL;
	ret->GDIPrint->undercmd = FALSE;
    ret->GDIPrint->esc = FALSE;
	ret->GDIPrint->fontType = 0;
    
	/* Creating Device context (Microsoft Win32 API (GDI)) */
    ret->GDIPrint->printerDC = CreateDC(NULL, printer, NULL, NULL);
	if (!ret->GDIPrint->printerDC) goto error;

	/* Getting Page sizes, Page offsets, Printer resolution and Device units scalling */
    /* Resolution */
	ret->GDIPrint->bitsPerInchX = GetDeviceCaps(ret->GDIPrint->printerDC, LOGPIXELSX);
    ret->GDIPrint->bitsPerInchY = GetDeviceCaps(ret->GDIPrint->printerDC, LOGPIXELSY);
    if (ret->GDIPrint->bitsPerInchY == 0) ret->GDIPrint->bitsPerInchY = 300;
    if (ret->GDIPrint->bitsPerInchX == 0) ret->GDIPrint->bitsPerInchX = 300;
    /* Scalling factor */
	ret->GDIPrint->xScale = (FLOAT)ret->GDIPrint->bitsPerInchX / 96.0f ;
    ret->GDIPrint->yScale = (FLOAT)ret->GDIPrint->bitsPerInchY / 96.0f ;
    /* Physical Font size by device */
	ret->GDIPrint->nInternalFontSize = -MulDiv(fontSize, GetDeviceCaps(ret->GDIPrint->printerDC, LOGPIXELSY), 72);
    /* Offsets */
	ret->GDIPrint->offsetX = GetDeviceCaps(ret->GDIPrint->printerDC, PHYSICALOFFSETX);
    ret->GDIPrint->offsetY = GetDeviceCaps(ret->GDIPrint->printerDC, PHYSICALOFFSETY);
    if (ret->GDIPrint->offsetY == 0) ret->GDIPrint->offsetY = (int)floor(20 * ret->GDIPrint->yScale);
    if (ret->GDIPrint->offsetX == 0) ret->GDIPrint->offsetX = (int)floor(20 * ret->GDIPrint->xScale);
	/* Paper size */
	ret->GDIPrint->paperHeight = GetDeviceCaps(ret->GDIPrint->printerDC, VERTSIZE) * 10;
	ret->GDIPrint->paperWidth = GetDeviceCaps(ret->GDIPrint->printerDC, HORZSIZE) * 10;
	if (ret->GDIPrint->paperHeight == 0) ret->GDIPrint->paperHeight = 2970;
    if (ret->GDIPrint->paperWidth == 0) ret->GDIPrint->paperWidth = 2100;
    /* Calculating printable area */
	ret->GDIPrint->paperHeight = ((ret->GDIPrint->paperHeight * ret->GDIPrint->bitsPerInchY) / 254) - (ret->GDIPrint->offsetY * 2);
    ret->GDIPrint->paperWidth = ((ret->GDIPrint->paperWidth * ret->GDIPrint->bitsPerInchX) / 254) - (ret->GDIPrint->offsetX * 2);
    
	/* Preparing document description */
	ret->GDIPrint->docInfo.cbSize = sizeof(ret->GDIPrint->docInfo);
    ret->GDIPrint->docInfo.lpszDocName = "PuTTY Document";
    ret->GDIPrint->docInfo.lpszOutput = NULL;
    ret->GDIPrint->docInfo.lpszDatatype = "RAW";
    ret->GDIPrint->docInfo.fwType = 0;

	/* Start document */
	status = StartDoc(ret->GDIPrint->printerDC, &ret->GDIPrint->docInfo);
    if (status <= 0) goto error;
    docstarted = 1;
	/* Start first page */
    status = StartPage(ret->GDIPrint->printerDC);
    if (status <= 0) goto error;
    pagestarted = 1;

	/* Creating all needed fonts: Draft, Draft Condensed, Bold, Bold Condensed (as on Dotmatrix printers) */
	ret->GDIPrint->fontN = CreateFont(ret->GDIPrint->nInternalFontSize, 0, 0, 0, FW_NORMAL, FALSE, FALSE, FALSE, charSet, outPrecis, clipPrecis, fontQuality, fontPitch, (LPCSTR)fontName);
    if (ret->GDIPrint->fontN)
      SelectObject(ret->GDIPrint->printerDC, ret->GDIPrint->fontN);
    else
      goto error;
    if (!GetTextMetrics(ret->GDIPrint->printerDC,&ret->GDIPrint->textmetric)) goto error;
    ret->GDIPrint->nFontHeight = ret->GDIPrint->textmetric.tmHeight;
    ret->GDIPrint->nFontWidth = ret->GDIPrint->textmetric.tmAveCharWidth;
    ret->GDIPrint->cFontHeight = ret->GDIPrint->nFontHeight;
    ret->GDIPrint->cFontWidth = (int)floor(ret->GDIPrint->nFontWidth * .58);  /* Scalling factor for condensed font - 58% */
	ret->GDIPrint->lFontHeight = ret->GDIPrint->nFontHeight;
	ret->GDIPrint->lFontWidth = ret->GDIPrint->nFontWidth * 2;                /* Scalling factor for wide - 200% */
    
	ret->GDIPrint->fontC = CreateFont(ret->GDIPrint->cFontHeight, ret->GDIPrint->cFontWidth, 0, 0, FW_NORMAL, FALSE, FALSE, FALSE, charSet, outPrecis, clipPrecis, fontQuality, fontPitch, (LPCSTR)fontName);
    ret->GDIPrint->fontL = CreateFont(ret->GDIPrint->lFontHeight, ret->GDIPrint->lFontWidth, 0, 0, FW_NORMAL, FALSE, FALSE, FALSE, charSet, outPrecis, clipPrecis, fontQuality, fontPitch, (LPCSTR)fontName);
	ret->GDIPrint->fontNB = CreateFont(ret->GDIPrint->nFontHeight, ret->GDIPrint->nFontWidth, 0, 0, FW_BOLD, FALSE, FALSE, FALSE, charSet, outPrecis, clipPrecis, fontQuality, fontPitch, (LPCSTR)fontName);
    ret->GDIPrint->fontCB = CreateFont(ret->GDIPrint->cFontHeight, ret->GDIPrint->cFontWidth, 0, 0, FW_BOLD, FALSE, FALSE, FALSE, charSet, outPrecis, clipPrecis, fontQuality, fontPitch, (LPCSTR)fontName);
    ret->GDIPrint->fontLB = CreateFont(ret->GDIPrint->lFontHeight, ret->GDIPrint->lFontWidth, 0, 0, FW_BOLD, FALSE, FALSE, FALSE, charSet, outPrecis, clipPrecis, fontQuality, fontPitch, (LPCSTR)fontName);
    ret->GDIPrint->fontNU = CreateFont(ret->GDIPrint->nFontHeight, ret->GDIPrint->nFontWidth, 0, 0, FW_NORMAL, FALSE, TRUE, FALSE, charSet, outPrecis, clipPrecis, fontQuality, fontPitch, (LPCSTR)fontName);
    ret->GDIPrint->fontCU = CreateFont(ret->GDIPrint->cFontHeight, ret->GDIPrint->cFontWidth, 0, 0, FW_NORMAL, FALSE, TRUE, FALSE, charSet, outPrecis, clipPrecis, fontQuality, fontPitch, (LPCSTR)fontName);
    ret->GDIPrint->fontLU = CreateFont(ret->GDIPrint->lFontHeight, ret->GDIPrint->lFontWidth, 0, 0, FW_NORMAL, FALSE, TRUE, FALSE, charSet, outPrecis, clipPrecis, fontQuality, fontPitch, (LPCSTR)fontName);
    ret->GDIPrint->fontNUB = CreateFont(ret->GDIPrint->nFontHeight, ret->GDIPrint->nFontWidth, 0, 0, FW_BOLD, FALSE, TRUE, FALSE, charSet, outPrecis, clipPrecis, fontQuality, fontPitch, (LPCSTR)fontName);
    ret->GDIPrint->fontCUB = CreateFont(ret->GDIPrint->cFontHeight, ret->GDIPrint->cFontWidth, 0, 0, FW_BOLD, FALSE, TRUE, FALSE, charSet, outPrecis, clipPrecis, fontQuality, fontPitch, (LPCSTR)fontName);
    ret->GDIPrint->fontLUB = CreateFont(ret->GDIPrint->lFontHeight, ret->GDIPrint->lFontWidth, 0, 0, FW_BOLD, FALSE, TRUE, FALSE, charSet, outPrecis, clipPrecis, fontQuality, fontPitch, (LPCSTR)fontName);

	/* Setting initial print position */
	SetTextAlign(ret->GDIPrint->printerDC, (UINT)(TA_BOTTOM | TA_LEFT));
    ret->GDIPrint->pixelPosX = ret->GDIPrint->offsetX;
    ret->GDIPrint->posX = 0;
    ret->GDIPrint->posY = 0;
	ret->GDIPrint->tenPos[0] = ret->GDIPrint->offsetX + ret->GDIPrint->cFontWidth;
	for (i=1; i<FIXED_TABS; i++) ret->GDIPrint->tenPos[i] = ret->GDIPrint->tenPos[i-1] + ret->GDIPrint->cFontWidth;
	
	/* Success Done */
	return ret;

	/* Error. Need to release memory and return NULL */
error:
    if (pagestarted)
	    EndPage(ret->GDIPrint->printerDC);
    if (docstarted)
	    EndDoc(ret->GDIPrint->printerDC);
    if (ret->GDIPrint->printerDC)
	    DeleteDC(ret->GDIPrint->printerDC);
	if (ret->GDIPrint->fontN)
        DeleteObject(ret->GDIPrint->fontN);
	if (ret->GDIPrint->fontC)
        DeleteObject(ret->GDIPrint->fontC);
	if (ret->GDIPrint->fontL)
        DeleteObject(ret->GDIPrint->fontL);
	if (ret->GDIPrint->fontNB)
        DeleteObject(ret->GDIPrint->fontNB);
	if (ret->GDIPrint->fontCB)
        DeleteObject(ret->GDIPrint->fontCB);
	if (ret->GDIPrint->fontLB)
        DeleteObject(ret->GDIPrint->fontLB);
	if (ret->GDIPrint->fontNU)
        DeleteObject(ret->GDIPrint->fontNU);
	if (ret->GDIPrint->fontCU)
        DeleteObject(ret->GDIPrint->fontCU);
	if (ret->GDIPrint->fontLU)
        DeleteObject(ret->GDIPrint->fontLU);
	if (ret->GDIPrint->fontNUB)
        DeleteObject(ret->GDIPrint->fontNUB);
	if (ret->GDIPrint->fontCUB)
        DeleteObject(ret->GDIPrint->fontCUB);
	if (ret->GDIPrint->fontLUB)
        DeleteObject(ret->GDIPrint->fontLUB);
	sfree(ret->GDIPrint);
    sfree(ret);
    return NULL;
}

/* Internal function (Change font) */
void change_font(printer_job *pj)
{
	switch (pj->GDIPrint->fontType)
	{
	case ftN:
		if (pj->GDIPrint->fontN) SelectObject(pj->GDIPrint->printerDC, pj->GDIPrint->fontN);
		break;
	case ftNU:
		if (pj->GDIPrint->fontNU) SelectObject(pj->GDIPrint->printerDC, pj->GDIPrint->fontNU);
		break;
	case ftNB:
		if (pj->GDIPrint->fontNB) SelectObject(pj->GDIPrint->printerDC, pj->GDIPrint->fontNB);
		break;
	case ftNUB:
		if (pj->GDIPrint->fontNUB) SelectObject(pj->GDIPrint->printerDC, pj->GDIPrint->fontNUB);
		break;
	case ftC:
		if (pj->GDIPrint->fontC) SelectObject(pj->GDIPrint->printerDC, pj->GDIPrint->fontC);
		break;
	case ftCU:
		if (pj->GDIPrint->fontCU) SelectObject(pj->GDIPrint->printerDC, pj->GDIPrint->fontCU);
		break;
	case ftCB:
		if (pj->GDIPrint->fontCB) SelectObject(pj->GDIPrint->printerDC, pj->GDIPrint->fontCB);
		break;
	case ftCUB:
		if (pj->GDIPrint->fontCUB) SelectObject(pj->GDIPrint->printerDC, pj->GDIPrint->fontCUB);
		break;
	case ftL:
		if (pj->GDIPrint->fontL) SelectObject(pj->GDIPrint->printerDC, pj->GDIPrint->fontL);
		break;
	case ftLU:
		if (pj->GDIPrint->fontLU) SelectObject(pj->GDIPrint->printerDC, pj->GDIPrint->fontLU);
		break;
	case ftLB:
		if (pj->GDIPrint->fontLB) SelectObject(pj->GDIPrint->printerDC, pj->GDIPrint->fontLB);
		break;
	case ftLUB:
		if (pj->GDIPrint->fontLUB) SelectObject(pj->GDIPrint->printerDC, pj->GDIPrint->fontLUB);
		break;
	default:
		break;
	}
  return;
}

/* Printing block of data to GDI printer */
void printer_job_data_gdi(printer_job *pj, void *data, int len)
{
    int status = 0;
    int i,j,w,h,nsp;
    char *str;

	/* If printing not initialized - Run, Forrest! Run! */
	if (!pj)
      return;
    
    /* Represent void* data block as string (Possible because We known about data length) */
	str = (char *) data;
	/* For all chars in block. Assembling our puzzle char by char
	   Recognized control characters and escape sequences:
	   #7  - Next of 1/10 paper position
	   #9  - Tabulation (x8)
	   #12 - New page
	   #13 - New line
	   #14 - Double wide font
	   #15 - Condensed font
	   #18 - Normal font
	   #27 #69 - Bold weight
	   #27 #70 - Normal weight
	   #27 #45 #0 - Underline OFF
	   #27 #45 #1 - Underline ON
	   #27 #45 #48 - Underline OFF
	   #27 #45 #49 - Underline ON
	*/
    for (i=0; i<len; i++) {
      
	  /* ESC Char */
      if ((byte)str[i] == 27) {
        pj->GDIPrint->esc = TRUE;
        continue;
      }
      
	  /* Char #14 standalone */
	  if (((byte)str[i] == 14) && (!pj->GDIPrint->esc)) {
		  pj->GDIPrint->fontType = (pj->GDIPrint->fontType & ftNoCondens);
		  pj->GDIPrint->fontType = (pj->GDIPrint->fontType | ftLarge);
		  change_font(pj);
		  continue;
	  }

	  /* Char #15 standalone */
	  if (((byte)str[i] == 15) && (!pj->GDIPrint->esc)) {
		  pj->GDIPrint->fontType = (pj->GDIPrint->fontType | ftCondens);
		  pj->GDIPrint->fontType = (pj->GDIPrint->fontType & ftNoLarge);
		  change_font(pj);
		  continue;
	  }
      
	  /* Char #18 standalone */
	  if (((byte)str[i] == 18) && (!pj->GDIPrint->esc)) {
		  pj->GDIPrint->fontType = (pj->GDIPrint->fontType & ftNoCondens);
		  pj->GDIPrint->fontType = (pj->GDIPrint->fontType & ftNoLarge);
		  change_font(pj);
		  continue;
	  }
      
	  /* Underline request */
	  if (pj->GDIPrint->undercmd && (!pj->GDIPrint->esc)) {
		  if (((byte)str[i] == 1) || ((byte)str[i] == 49)) pj->GDIPrint->fontType = (pj->GDIPrint->fontType | ftUnderline);
		  else pj->GDIPrint->fontType = (pj->GDIPrint->fontType & ftNoUnderline);
		  pj->GDIPrint->undercmd = FALSE;
		  change_font(pj);
		  continue;
	  }
	  
	  /* ESC command request */
	  if (pj->GDIPrint->esc) {
        if ((byte)str[i] == 69) {
          pj->GDIPrint->fontType = (pj->GDIPrint->fontType | ftBold);
		  change_font(pj);
        }
        if ((byte)str[i] == 70) {
          pj->GDIPrint->fontType = (pj->GDIPrint->fontType & ftNoBold);
		  change_font(pj);
        }
		if ((byte)str[i] == 45) {
          pj->GDIPrint->undercmd = TRUE;
        }
		else {
		  pj->GDIPrint->undercmd = FALSE;
		}
        pj->GDIPrint->esc = FALSE;
        continue;
      }

	  /* Char #13 standalone */
      if ((byte)str[i] == 13) {
        pj->GDIPrint->posY++;
        pj->GDIPrint->posX = 0;
        pj->GDIPrint->pixelPosX = pj->GDIPrint->offsetX;
        if (((pj->GDIPrint->posY * pj->GDIPrint->nFontHeight) + pj->GDIPrint->offsetY) > pj->GDIPrint->paperHeight) {
          status = EndPage(pj->GDIPrint->printerDC);
          status = StartPage(pj->GDIPrint->printerDC);
          pj->GDIPrint->posY = 0;
          pj->GDIPrint->posX = 0;
          pj->GDIPrint->pixelPosX = pj->GDIPrint->offsetX;
        }
        continue;
      }

	  /* Char #12 standalone */
	  if ((byte)str[i] == 12) {
        status = EndPage(pj->GDIPrint->printerDC);
        status = StartPage(pj->GDIPrint->printerDC);
        pj->GDIPrint->posY = 0;
        pj->GDIPrint->posX = 0;
        pj->GDIPrint->pixelPosX = pj->GDIPrint->offsetX;
        continue;
      }
      
      /* Character Width and Height calculation */
	  if ((pj->GDIPrint->fontType & (ftL+ftC)) == 0) {
          w = pj->GDIPrint->nFontWidth;
          h = pj->GDIPrint->nFontHeight;
	  }
	  else {
		  if ((pj->GDIPrint->fontType & ftL) > 0) {
            w = pj->GDIPrint->lFontWidth;
            h = pj->GDIPrint->lFontHeight;;
		  }
		  else {
            w = pj->GDIPrint->cFontWidth;
            h = pj->GDIPrint->cFontHeight;;
		  }
	  }

      /* TAB */
	  if ((byte)str[i] == 9) {
        nsp = 8 - ((1 + pj->GDIPrint->posX) % 8);
        for (j=1; j<=nsp; j++) {
          /* TextOutA (pj->GDIPrint->printerDC, pj->GDIPrint->pixelPosX, pj->GDIPrint->offsetY+(h * pj->GDIPrint->posY), " ", 1); */
          pj->GDIPrint->pixelPosX = pj->GDIPrint->pixelPosX + w;
          pj->GDIPrint->posX++;
        }
		for (j=0; j<FIXED_TABS; j++) {
			if (pj->GDIPrint->tenPos[j] == pj->GDIPrint->pixelPosX) break;
			if (pj->GDIPrint->tenPos[j] > pj->GDIPrint->pixelPosX) {
				pj->GDIPrint->pixelPosX = pj->GDIPrint->tenPos[j];
				break;
			}
		}
        continue;
      }

	  /* Other control characters. Ignored */
      if ((byte)str[i] == 127) continue;
      if ((byte)str[i] < 32) continue;

	  /* Out next character */
	  status = TextOutA (
		pj->GDIPrint->printerDC,
		pj->GDIPrint->pixelPosX,
		pj->GDIPrint->offsetY+(h * pj->GDIPrint->posY),
		str +i,
		1);
      pj->GDIPrint->pixelPosX = pj->GDIPrint->pixelPosX + w;
      pj->GDIPrint->posX++;
    }

}

/* End GDI printing */
void printer_finish_job_gdi(printer_job *pj)
{
	/* If printing not initialized - Run, Forrest! Run! */
    if (!pj)
      return;

	/* Closing Page, Closing Document */
    EndPage(pj->GDIPrint->printerDC);
    EndDoc(pj->GDIPrint->printerDC);
    
	/* Releasing memory */
	if (pj->GDIPrint->printerDC)
	    DeleteDC(pj->GDIPrint->printerDC);
	if (pj->GDIPrint->fontN)
        DeleteObject(pj->GDIPrint->fontN);
	if (pj->GDIPrint->fontC)
        DeleteObject(pj->GDIPrint->fontC);
	if (pj->GDIPrint->fontL)
        DeleteObject(pj->GDIPrint->fontL);
	if (pj->GDIPrint->fontNB)
        DeleteObject(pj->GDIPrint->fontNB);
	if (pj->GDIPrint->fontCB)
        DeleteObject(pj->GDIPrint->fontCB);
	if (pj->GDIPrint->fontLB)
        DeleteObject(pj->GDIPrint->fontLB);
	if (pj->GDIPrint->fontNU)
        DeleteObject(pj->GDIPrint->fontNU);
	if (pj->GDIPrint->fontCU)
        DeleteObject(pj->GDIPrint->fontCU);
	if (pj->GDIPrint->fontLU)
        DeleteObject(pj->GDIPrint->fontLU);
	if (pj->GDIPrint->fontNUB)
        DeleteObject(pj->GDIPrint->fontNUB);
	if (pj->GDIPrint->fontCUB)
        DeleteObject(pj->GDIPrint->fontCUB);
	if (pj->GDIPrint->fontLUB)
        DeleteObject(pj->GDIPrint->fontLUB);
    sfree(pj->GDIPrint);
    sfree(pj);
}
